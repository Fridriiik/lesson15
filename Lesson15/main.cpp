#include <iostream>

constexpr auto CR = "\n";


void EvenOdd(int i, int n) {
	for (i; i <= n; i += 2) {
			std::cout << i << CR;
	}

}


int main() {

	const int n = 100; 

	for (int i = 0; i <= n; i++) {
		if ((i % 2) == 0) {
			std::cout << i << CR;
		}
	}

	EvenOdd(1, 100);

	return 0; 
}